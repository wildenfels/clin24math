<?php
/**
 * File Division.php divides num1 by num2 and rounds to two decimals.
 * 
 * PHP Version 8
 * 
 * @category  PHP
 * @package   Mibeg
 * @author    Alexander, Sarah, Nadine <we@somewhere.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de
 */

/**
 * Class Division divides num1 by num2
 * 
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Alexander, Sarah, Nadine <we@somewhere.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de
 */
class Division implements iCalc
{
    /**
     * Methode calc divides num1 by num2 and rounds to two decimals.
     * 
     * @param float $num1 erste Zahl $num1
     * @param float $num2 zweite Zahl $num2
     * 
     * @return  float (0.00)
     * @access  public
     * @details In allen Rechen-Klassen calc Methode
     */
    public function calc($num1, $num2)
    {
        return round(($num1 / $num2), 2);
    }
}


