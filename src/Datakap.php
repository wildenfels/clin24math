<?php
/**
 * File Datakap.php it for welcoming the customer
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Elena, Massudah, Philip <my@email.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de/20210119/clin24math
 */

/**
 * Class Datakap.php it for welcoming the customer
 *
 * PHP Version 8
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Elena, Massudah, Philip <my@email.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de/20210119/clin24math
 */
 
class Datakap
{
    /**
     * This is the input of integer 1
     *
     * @var    integer
     * @access protected
     */
    protected $int1= 0;
    
    /**
     * This is the input of integer 2
     *
     * @var    integer
     * @access protected
     */
    protected $int2= 0;
    
    /**
     * This is the input of string 1
     *
     * @var    string
     * @access protected
     */
    public $str1= "";

    
    /**
     * Method contructor
     *
     * @return void
     * @access public
     */
    public function __construct($platzhalter1, $zahl2, $text)
    {
        $this->setInt1($platzhalter1);
        $this->setInt2($zahl2);
        $this->setStr1($text);
    }
    
    /**
     * Method setInt1
     *
     * @return void
     * @access public
     */
    protected function setInt1($platzhalter1a)
    {
        $this->$int1 = intval($platzhalter1a);
    }
    
    /**
     * Method getInt1
     *
     * @return integer
     * @access public
     */
    public function getInt1()
    {
        return $this->$int1;
    }
    
    /**
     * Method setInt2
     *
     * @return void
     * @access public
     */
    protected function setInt2($platzhalter2a)
    {
        $this->$int2 = intval($platzhalter2a);
    }
    
    /**
     * Method getInt2
     *
     * @return integer
     * @access public
     */
    public function getInt2()
    {
        return $this->$int2;
    }
    
    /**
     * Method setStr1
     *
     * @return void
     * @access public
     */
    public function setStr1($platzhalter3a)
    {
        $this->$str1 = strip_tags($platzhalter3a);
    }
    
    /**
     * Method getStr1
     *
     * @return string
     * @access public
     */
    public function getStr1()
    {
        return $this->$str1;
    }
}//end class
