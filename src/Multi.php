<?php
/**
 * File Multi.php multipliziert zwei Zahlen
 *
 * PHP version 8
 *
 * @category  PHP
 * @package   Clindat24
 * @author    Jana Foerster
 * @copyright 2021 Clindat24
 * @license   BSD-3 https://opensource.org
 * @link      http://clindat.mibeg-cms.de/
 */

/**
 * Class Multi.php multipliziert zwei Zahlen
 *
 * PHP version 8
 *
 * @category  PHP
 * @package   Clindat24
 * @author    Jana Foerster
 * @copyright 2021 Clindat24
 * @license   BSD-3 https://opensource.org
 * @link      http://clindat.mibeg-cms.de/
 */

class Multi implements iCalc
{
    /**
     * Number for multiplication, $zahl1.
     *
     * @var    float
     * @access private
     */
    private $zahl1 = 0.00; //Eigenschaft $zahl1 als Zahl festlegen
    
    /**
     * Number for division, $zahl.
     *
     * @var    float
     * @access private
     */
    private $zahl2 = 0.00; //Eigenschaft $zahl2 als Zahl festlegen
    
     /**
     * Number for division, $zahl.
     *
     * @var    float
     * @access private
     */
    private $multi_ergebnis = 0.00; //Eigenschaft $zahl2 als Zahl festlegen
    
    /**
     * Function durch100 divides incoming number by 100 and rounds to two decimals.
     Returns number as float.
     *
     * @param integer $zahl_ext (incoming number).
     *
     * @return float
     * @access public
     */
    public function calc($zahl1_ext, $zahl2_ext)
    {
        $this->zahl1 = $zahl1_ext; //eingehende Zahl1 auslesen
        $this->zahl2 = $zahl2_ext; //eingehende Zahl2 auslesen
        
        $this->multi_ergebnis = $this->zahl1 * $this->zahl2;
        
        return $this->multi_ergebnis; //Rückgabe des Werts von Zahl
    }//end calc
}//end class

