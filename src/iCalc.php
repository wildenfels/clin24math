<?php
/**
 * Datei iCalc Schnittstelle für Rechen-Klassen
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Alex zu Solms <alex@dom.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de
 */
 
/**
 * Interface iCalc Schnittstelle für Rechen-Klassen
 *
 * PHP Version 7
 *
 * @category  PHP
 * @package   Mibeg
 * @author    Alex zu Solms <alex@dom.de>
 * @copyright 2021 Mibeg
 * @license   BSD License
 * @link      http://clindat.mibeg-cms.de
 */
interface iCalc
{
    /**
     *  @brief Methode calc 
     *  
     *  @param float $num1 erste Zahl $num1
     *  @param float $num2 zweite Zahl $num2
     *  @return Return description
     *  
     *  @details In allen Rechen-Klassen calc Methode
     */
    public function calc($num1, $num2);	
}