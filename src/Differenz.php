<?php
include_once 'iCalc.php';
class Differenz implements iCalc
{    
    public function calc($num1, $num2) {
        $zahl1 = strip_tags($num1);
        $zahl2 = strip_tags($num2);
        return $zahl1 - $zahl2;
    }
}
$diff=new Differenz();
//echo $diff->calc(10.26, 5.39);