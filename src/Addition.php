<?php

/**
 * Datei Addition.php addiert zwei Zahlen und rundet nach zwei Kommastellen
 *
 * PHP Version 8
 *
 * @category PHP
 * @package Mibeg
 * @author Cihan Guemues, Jonatan Woerle <mail@mail.net>
 * @copyright 2021 Mibeg
 * @license BSD License
 * @link http://clindat.mibeg-cms.de/20210119/...
 */


/**
 * Klasse Addition.php addiert zwei Zahlen und rundet nach zwei Kommastellen
 *
 * PHP Version 8
 *
 * @category PHP
 * @package Mibeg
 * @author Cihan Guemues, Jonatan Woerle <mail@mail.net>
 * @copyright 2021 Mibeg
 * @license BSD License
 * @link http://clindat.mibeg-cms.de/20210119/...
 */

class Addition implements iCalc
{
    /**
     * Method calc
     *
     * @param integer $num1 von num1
     * @param float $num2 von num2

     * @return float (0.00)
     * @access public
     */
    public function calc($num1, $num2)
    {
        return number_format($num1 + $num2, 2);
    }
}